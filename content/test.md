---
title: "Title"
subtitle: "Subtitle"
author: "Florian Kohrt"
date: "17. Juni 2020"
lang: en
---

# A level-one heading with a [link](/url) and *emphasis*

A paragraph is one or more lines of text followed by one or more blank lines. Newlines are treated as spaces, so you can reflow your paragraphs as you like. If you need a hard line break, put two or more spaces at the end of a line.

# Block quotations

> Once upon a time, in a distant
> galaxy called Ööç,
> there lived a computer
> named R.~J. Drofnats.
> 
> Mr.~Drofnats---or "R. J.," as
> he preferred to be called---
> was happiest when he was at work
> typesetting beautiful documents.
>
> 1. This is a list inside a block quote.
> 2. Second item.
>
> > A block quote within a block quote.
>
>     code

# Verbatim (code) blocks

```{.java .numberLines .lineAnchors startFrom="100"}
public static void main(String[] args) {
    System.out.println("Hello World!");
}
```

# Line blocks

| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical

# Lists

## Bullet lists

* fruits
  + apples
    - macintosh
    - red delicious
  + pears
  + peaches
* vegetables
  + broccoli
  + chard

## Ordered lists

1.  one
2.  two
3.  three

## Task lists

- [ ] an unchecked task list item
- [x] checked item

## Definition lists

Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b

## Numbered example lists

(@good)  This is a good example.

As (@good) illustrates, ...

# Horizontal rules

---

# Tables

fruit| price
-----|-----:
apple|2.05
pear|1.37
orange|3.09

# Inline formatting

Syntax|Output
--|--
`_Emphasis_`|_Emphasis_
`__Strong Emphasis__`|__Strong Emphasis__
`2^10^ is 1024.`|2^10^ is 1024.
`H~2~O is a liquid.`|H~2~O is a liquid.
`This ~~is deleted text.~~`|This ~~is deleted text.~~
`` `Inline code` with backticks ``|`Inline code` with backticks
`[Underline]{.underline}`|[Underline]{.underline}
`[Small caps]{.smallcaps}`|[Small caps]{.smallcaps}
`[Highlight]{.mark}`|[Highlight]{.mark}

## Alternatives to the asterisk

Sometimes the asterisk (`*`) can be replaced by an underscore (`_`) or a dash (`-`).

```md
This text is _emphasized with underscores_, and this is *emphasized with asterisks*.

This is **strong emphasis** and __with underscores__.
```

```md
* bullets
* with
* asterisks
```

```md
- dashes
- work
- too
```

Horizontal rules: `***` or `---`

### Inline code attributes

`<$>`{.haskell}

# Math


# Links

<https://google.com>
<sam@green.eggs.ham>

This is an [inline link](/url), and here's [one with
a title](https://fsf.org "click here for a good time!").

[Write me!](mailto:sam@green.eggs.ham)

# Images

![la lune](lalune.jpg "Voyage to the moon")

An inline ![image](foo.jpg){#id .class width=30 height=20px}.

# Divs and Spans

::::: {#special .sidebar}
Here is a paragraph.

And another.
:::::

[This is *some text*]{.class key="val"}

# Footnotes

Here is a footnote reference [^longnote].

Here is an inline note.^[Inlines notes are easier to write, since
you don't have to pick an identifier and move down to type the
note.]

# Citations

